//
//  DetailsViewModel.swift
//  Guidomia_Bell_Project
//
//  Created by logesh on 4/22/21.
//  Copyright © 2021 logesh. All rights reserved.
//

import Foundation

struct DetailsModel {
    var Cons : [String]?
    var Pros : [String]?
    
    
    func getSectionCount() -> Int{
        return 2
    }
    
    func getDetailsForSection(section : DetailsSection,data : GuidomiaModel)-> [String] {
        switch section {
        case .Cons:
            return data.consList?.filter({$0 != ""}) ?? []
        case .Pros:
            return data.prosList?.filter({$0 != ""}) ?? []
        }
    }
    
    func getNumberofRowsForSection(section : DetailsSection,detailsContent : GuidomiaModel) -> Int{
        switch section{
        case .Cons:
            return detailsContent.consList?.filter({$0 != ""}).count ?? 0
        case .Pros:
            return detailsContent.prosList?.filter({$0 != ""}).count ?? 0
        }
        
    }
    
    
}

enum DetailsSection: Int {
    case Pros = 0
    case Cons = 1
    
    var title : String {
        switch self {
        case .Pros:
            return "Pros:"
        case .Cons:
            return "Cons:"
        }
        
        
        
    }
   
}
