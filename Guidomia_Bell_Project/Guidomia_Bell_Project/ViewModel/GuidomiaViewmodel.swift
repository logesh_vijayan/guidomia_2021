//
//  GuidomiaViewmodel.swift
//  Guidomia_Bell_Project
//
//  Created by logesh on 4/22/21.
//  Copyright © 2021 logesh. All rights reserved.
//

import Foundation
import UIKit

//MARK:- Class
class GuidomiaViewModel: NSObject {
    
//MARK:- Variables
    lazy var manager = GuidomiaManager()
    var guidomiaData : [GuidomiaModel] = []
    var filteredContent : [GuidomiaModel] = []
    //MARK:Initialiser
    override init() {
        super.init()
        getCarData()
    }

    //MARK: -Model  Function
    func  getDataFromJson() {
           manager.readJson({ guidomiaData,result in
               if result == .Sucess
               {
                   guard let data = guidomiaData
                   else{
                       self.guidomiaData = []
                       return
                   }
                   self.guidomiaData = data
                guard !self.guidomiaData.isEmpty else {
                    return
                }
                for data in guidomiaData!
                {
                    CoreStore.shared.savecarData(data: data)
                }
              
                
               }
               })
       }
    
    func getCarData()  {
          let data = CoreStore.shared.readData()
        guard !data.isEmpty else {
            getDataFromJson()
            return
        }
        for car in data {
            guidomiaData.append(GuidomiaModel(consList: car.cons, customerPrice: car.customerPrice, make: car.make!, marketPrice: car.marketPrice, model: car.model!, prosList: car.pros, rating: car.rating))
         
        }
    }
    
    func getFilterData(data: String){
        filteredContent = []
        let datatoFilter = data.lowercased()
        filteredContent = guidomiaData.filter({$0.make.lowercased().contains(datatoFilter) || $0.model.lowercased().contains(datatoFilter)})
        }
        
    func getHeightForView(data: IndexPath) -> CGFloat{
         let count = getContent(indexx: data).count
        return CGFloat(count) * 28
    }
 

    func getContent(indexx : IndexPath) -> [String]{
        var data = guidomiaData[indexx.row].consList! + guidomiaData[indexx.row].prosList!
        data = data.filter({$0 != ""})
        return data
    }
    
    
    
}
