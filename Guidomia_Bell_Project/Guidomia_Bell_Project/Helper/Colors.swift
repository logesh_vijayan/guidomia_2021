//
//  Colors.swift
//  Guidomia_Bell_Project
//
//  Created by logesh on 4/21/21.
//  Copyright © 2021 logesh. All rights reserved.
//

import Foundation
import UIKit

enum Color {
    
    case navigationBarColor
    case primaryTextColour
    case headerViewBackgroundColor
    case filterBackgroundColor
    case dataBackgroundColor
    
}



extension Color {
    
    var value: UIColor {
        var instanceColor = UIColor.clear
        
        switch self {
        case .navigationBarColor:
            instanceColor = UIColor(hexString: "#FC6016")
        case .primaryTextColour:
            instanceColor = UIColor(hexString: "#FFFFFF")
        case .headerViewBackgroundColor:
            instanceColor = UIColor(hexString: "#F7F7F7")
        case .filterBackgroundColor:
            instanceColor = UIColor(hexString: "#858585")
        case .dataBackgroundColor:
            instanceColor = UIColor(hexString: "#D5D5D5")
            
        }
        return instanceColor
    }
}



extension UIColor {
  
    convenience init(hexString: String) {
            
      let hexString: String = (hexString as NSString).trimmingCharacters(in: .whitespacesAndNewlines)
        let scanner : Scanner   = Scanner(string: hexString as String)

      if hexString.hasPrefix("#") {
        scanner.scanLocation = 1
      }
      var color: UInt32 = 0
      scanner.scanHexInt32(&color)
            
      let mask = 0x000000ff
      let r = Int(color >> 16) & mask
      let g = Int(color >> 8) & mask
      let b = Int(color) & mask
      
      let red   = CGFloat(r) / 255.0
      let green = CGFloat(g) / 255.0
      let blue  = CGFloat(b) / 255.0
      self.init(red:red, green:green, blue:blue, alpha:1)
    }
  

    convenience init(red: Int, green: Int, blue: Int) {
      assert(red >= 0 && red <= 255, "Invalid red component")
      assert(green >= 0 && green <= 255, "Invalid green component")
      assert(blue >= 0 && blue <= 255, "Invalid blue component")
      self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    
}

