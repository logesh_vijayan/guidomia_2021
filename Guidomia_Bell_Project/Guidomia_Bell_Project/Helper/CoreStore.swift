//
//  CoreStore.swift
//  Guidomia_Bell_Project
//
//  Created by logesh on 4/25/21.
//  Copyright © 2021 logesh. All rights reserved.
//

import Foundation
import CoreData

//MARK: - Class
class CoreStore: NSObject {
    
    //MARK: - Variables
  //  private var cars : [Car] = []
    private var cars : [Car] = []
    private var guidomiaModel : [GuidomiaModel] = []
    
    
    static let shared = CoreStore()
    
    var context:NSManagedObjectContext {
          return self.container.viewContext
      }

    private var container: NSPersistentContainer = {
        let persistentContainer = NSPersistentContainer(name: "Guidomia")
        persistentContainer.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError?
            {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return persistentContainer
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = container.viewContext
        if context.hasChanges {
            do {
                try context.save()
                print("Sucess")
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func readData() -> [Car] {

        let context = container.viewContext
        let request : NSFetchRequest<Car> = Car.fetchRequest()
        do{
         cars = try context.fetch(request)

        }
        catch{
            print(error)
        }
         return cars
    }

    func savecarData(data : GuidomiaModel)  {

        let carContext = Car(context : container.viewContext)
        carContext.cons = data.consList
        carContext.pros = data.prosList
        carContext.customerPrice = Int32(data.customerPrice)
        carContext.marketPrice = Int32(data.marketPrice)
        carContext.make = data.make
        carContext.model = data.model
        carContext.rating = Float(data.rating)
        self.saveContext()
    }

    
}
