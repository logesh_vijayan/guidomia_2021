//
//  GuidomiaManager.swift
//  Guidomia_Bell_Project
//
//  Created by logesh on 4/20/21.
//  Copyright © 2021 logesh. All rights reserved.
//

import Foundation

//MARK: - Class
class GuidomiaManager: NSObject {
    
    //MARK: - Initialiser Function
    override init() {
        super.init()
    }
    
    //MARK: - Parse funciton
    func readJson(_ completion: @escaping ( _ model : [GuidomiaModel]? ,_ result : ResultType) -> ()){
        
        if let path = Bundle.main.path(forResource: "car_list", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: [])
                let decoder = JSONDecoder()
                do {
                    let dataModel = try decoder.decode([GuidomiaModel].self, from: data)
                    completion(dataModel,.Sucess)
                }catch{
                    completion(nil,.failure)
                }
            } catch {
                completion(nil,.failure)
            }
        }
    }
    
    func getHeaderData() -> HeaderDataModel {
        return HeaderDataModel(headerImageName: "Tacoma", headerTitle: "Tacoma 2021", headerSubtitle: "Get Your's Now")
    }
    
}
