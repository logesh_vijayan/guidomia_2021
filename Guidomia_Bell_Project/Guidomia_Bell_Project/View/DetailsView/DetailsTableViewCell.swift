//
//  DetailsTableViewCell.swift
//  Guidomia_Bell_Project
//
//  Created by logesh on 4/22/21.
//  Copyright © 2021 logesh. All rights reserved.
//

import UIKit

class DetailsTableViewCell: UITableViewCell {

  
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var bulletImage: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialiseViews()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func initialiseViews(){
        self.backgroundColor = Color.dataBackgroundColor.value
        detailsLabel.font = .systemFont(ofSize: 14)
        detailsLabel.numberOfLines = 0
        detailsLabel.adjustsFontSizeToFitWidth = true
        bulletImage.tintColor = Color.navigationBarColor.value
    }
    
    func setupData(details : String){
        detailsLabel.text = details
    }
    
}
