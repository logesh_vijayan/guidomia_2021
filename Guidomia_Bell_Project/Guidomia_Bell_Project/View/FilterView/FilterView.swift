//
//  FilterView.swift
//  Guidomia_Bell_Project
//
//  Created by logesh on 4/21/21.
//  Copyright © 2021 logesh. All rights reserved.
//

import Foundation
import UIKit

protocol FilertoApplyDelegate : class {
    func searchCar(data : String?)
}


//MARK: - Class
class FilterView: UIView {
    
//MARK: - Outlets and Variables
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var filterBackgroundView: UIView!
    @IBOutlet weak var filterHeader: UILabel!
    @IBOutlet weak var makeTextfield: UITextField!
    @IBOutlet weak var modelTextfield: UITextField!
    weak var delegate :  FilertoApplyDelegate!
    //MARK: - View Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
         self.initScreen()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initScreen()
    }
    
    //MARK: -  Helper Methods
    private func initScreen(){
        setupContentView()
        configureViewAttributes()
        makeTextfield.delegate = self
        modelTextfield.delegate = self
    }
    
    private func setupContentView(){
        Bundle.main.loadNibNamed("FilterView", owner: self, options: nil)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(contentView)
    }
    
    private func configureViewAttributes(){
        filterBackgroundView.layer.cornerRadius = 10
        filterBackgroundView.backgroundColor = Color.filterBackgroundColor.value
        filterHeader.textColor = Color.primaryTextColour.value
        makeTextfield.layer.cornerRadius = 10
        modelTextfield.layer.cornerRadius = 10
        modelTextfield.backgroundColor = Color.primaryTextColour.value
        makeTextfield.backgroundColor = Color.primaryTextColour.value

    }
}

extension FilterView : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.delegate?.searchCar(data: textField.text)
        return textField.resignFirstResponder()
    }
    
}
