//
//  HeaderView.swift
//  Guidomia_Bell_Project
//
//  Created by logesh on 4/20/21.
//  Copyright © 2021 logesh. All rights reserved.
//

import UIKit

//MARK: - Class
class HeaderView: UIView {
    
//MARK: - Outlets and Variables
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var headerImageVIew: UIImageView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerSubTitleLabel: UILabel!
   
    var manager = GuidomiaManager()
    
    //MARK: - Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
         self.initScreen()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initScreen()
    }
    
    
    //MARK:- Initaliseer Function
    private func initScreen(){
        setupContentView()
        configureViewAttributes()
        getDataForHeader()
    }
    
    private func setupContentView(){
        Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        contentView.backgroundColor = UIColor.green
        addSubview(contentView)
    }
    
    private func configureViewAttributes(){
        headerTitleLabel.font = UIFont.boldSystemFont(ofSize: 25)
        headerSubTitleLabel.font = UIFont.boldSystemFont(ofSize: 14)
        headerTitleLabel.textColor = Color.primaryTextColour.value
        headerSubTitleLabel.textColor = Color.primaryTextColour.value
        headerImageVIew.contentMode = .scaleAspectFill
    }
    
    
    
    private func getDataForHeader(){
        let data = manager.getHeaderData()
        headerTitleLabel.text = data.headerTitle
        headerSubTitleLabel.text = data.headerSubtitle
        headerImageVIew.image = UIImage(named: data.headerImageName)
    }

}
