//
//  DataTableViewCell.swift
//  Guidomia_Bell_Project
//
//  Created by logesh on 4/21/21.
//  Copyright © 2021 logesh. All rights reserved.
//

import UIKit

//MARK :- Class
class DataTableViewCell: UITableViewCell {
    
    //MARK: - Outlets and Variables
    @IBOutlet weak var dataBackgroundView: UIView!
    @IBOutlet weak var dataImageView: UIImageView!
    @IBOutlet weak var dataTitle: UILabel!
    @IBOutlet weak var dataPrice: UILabel!
    @IBOutlet weak var starRatingView: StarRatingView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var prosconsTableView: UITableView!
  
    
    private var expandStatusSwitchHandler: (() -> Void) = {  }
      var dataModel : GuidomiaModel?
    lazy var detailsModel = DetailsModel()
    internal var isExpanded = false
    
    //MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        initialiseViewAttributes()
        addTapGestureRecogoniser()
        initaliseTableView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //MARK:- Class Functions
    func setupCellData(modelData : GuidomiaModel){
        
        dataImageView.image = UIImage(named:  modelData.getImageName(make: modelData.make, model: modelData.model))
        dataTitle.text = modelData.make
        dataPrice.text = "Price :\(Int((modelData.customerPrice/1000)))k"
        starRatingView.setStarsFor(rating: Float(modelData.rating))
        dataModel = modelData
    
        
    }
    
    private func initialiseViewAttributes(){
        
         self.selectionStyle = .none
        
        dataBackgroundView.backgroundColor = Color.dataBackgroundColor.value
        starRatingView.backgroundColor = Color.dataBackgroundColor.value
        separatorView.backgroundColor = Color.navigationBarColor.value
        
        dataTitle.textColor = Color.filterBackgroundColor.value
        dataPrice.textColor = Color.filterBackgroundColor.value
        
        dataTitle.font = UIFont.boldSystemFont(ofSize: 20)
        dataPrice.font = UIFont.boldSystemFont(ofSize: 12)
        
    }
    
    
    private func  initaliseTableView() {
        prosconsTableView.delegate = self
        prosconsTableView.dataSource = self
        prosconsTableView.register(UINib(nibName: "DetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "ProsCons")
        prosconsTableView.isScrollEnabled = false
        prosconsTableView.separatorStyle = .none
        prosconsTableView.backgroundColor = Color.dataBackgroundColor.value
    }
    
    private func addTapGestureRecogoniser() {
        
       let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled = true
    }
    
    @objc func handleTap() {
        expandStatusSwitchHandler()
    }
    
    func expandStatusSwitch( _ completionHandler : @escaping () -> Void) {
        expandStatusSwitchHandler = completionHandler
        prosconsTableView.reloadData()
    }
    
}


extension DataTableViewCell : UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailsModel.getNumberofRowsForSection(section:DetailsSection(rawValue: section)!, detailsContent: dataModel!)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = detailsModel.getDetailsForSection(section: DetailsSection(rawValue: indexPath.section)!, data: dataModel!)
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProsCons", for: indexPath) as? DetailsTableViewCell
        cell?.setupData(details: data[indexPath.row])
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 50))
        let labelblHeader = UILabel.init(frame: CGRect(x: 25, y: 13, width: tableView.bounds.size.width - 10, height: 24))
            labelblHeader.text = DetailsSection(rawValue: section)?.title
        labelblHeader.font = .systemFont(ofSize: 18)
            labelblHeader.textColor = Color.filterBackgroundColor.value
            headerView.addSubview(labelblHeader)
           return headerView
    }
    
    
    
}


extension DataTableViewCell : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}
