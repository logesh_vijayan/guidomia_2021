//
//  ViewController.swift
//  Guidomia_Bell_Project
//
//  Created by logesh on 4/20/21.
//  Copyright © 2021 logesh. All rights reserved.
//

import UIKit

//MARK: - Class
class GuidomiaViewController: UIViewController {
    
    //MARK: - Outlets and Variables
    @IBOutlet weak var navigationBarTitle: UINavigationBar!
    @IBOutlet weak var mainHeaderView: UIView!
    @IBOutlet weak var contentFilterView: FilterView!
    @IBOutlet weak var mainTableView: UITableView!
    
     var guidomiaViewModel = GuidomiaViewModel()
    var  isFiltered : Bool  = false
    private var cellToCollapse : [IndexPath] = []
    private var isDefaultrowExpanded  = false
    

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    //MARK:- Class Functions
    private func setupTableView(){
        mainTableView.delegate = self
        mainTableView.dataSource = self
        mainTableView.separatorStyle = .none
        navigationBarTitle.barTintColor = Color.navigationBarColor.value
        navigationBarTitle.delegate = self
        navigationBarTitle.isTranslucent = false
        mainTableView.register(UINib(nibName: "DataTableViewCell", bundle: nil), forCellReuseIdentifier: "DataCell")
        contentFilterView.delegate = self
    }
    
}

//MARK:- TableView Delegate
extension GuidomiaViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let isExpanded = cellToCollapse.contains(indexPath)
        var expandedCellHeight :CGFloat = 200.0
        if (isExpanded){
            expandedCellHeight = expandedCellHeight +  CGFloat(Double(guidomiaViewModel.getHeightForView(data: indexPath)))
        }
        return isExpanded ? UITableView.automaticDimension + expandedCellHeight : UITableView.automaticDimension
    }
}


//MARK:- TableView Data Source
extension GuidomiaViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltered ? guidomiaViewModel.filteredContent.count :   guidomiaViewModel.guidomiaData.count
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
       let cell =  tableView.dequeueReusableCell(withIdentifier: "DataCell", for: indexPath) as? DataTableViewCell
        cell?.setupCellData(modelData: isFiltered ? guidomiaViewModel.filteredContent[indexPath.row] : guidomiaViewModel.guidomiaData[indexPath.row])
        cell?.expandStatusSwitch{ 
                self.cellToCollapse.append(indexPath)
                self.cellToCollapse.remove(at: 0)
                self.mainTableView.reloadData()
                tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
        if !isDefaultrowExpanded {
            self.cellToCollapse = [IndexPath(row: 0, section: 0)]
            isDefaultrowExpanded = true
        }
        
        return cell ?? UITableViewCell()
    }
    
}

extension GuidomiaViewController : FilertoApplyDelegate {
    func searchCar(data: String?) {
        isFiltered = data == "" ? false : true
        guidomiaViewModel.getFilterData(data: data!)
        mainTableView.reloadData()
    }
    
}
    
    
    extension GuidomiaViewController : UINavigationBarDelegate {
        
        func position(for bar: UIBarPositioning) -> UIBarPosition {
            return .topAttached
        }
    }
    


