//
//  GuidomiaModel.swift
//  Guidomia_Bell_Project
//
//  Created by logesh on 4/20/21.
//  Copyright © 2021 logesh. All rights reserved.
//

import Foundation

struct GuidomiaModel : Codable {
    var consList : [String]?
    var customerPrice : Int32
    var make : String
    var marketPrice : Int32
    var model : String
    var prosList : [String]?
    var rating : Float
    
    
 func getImageName( make: String , model : String) -> String {
         return make+"_"+model
     }
}

struct HeaderDataModel {
    var headerImageName : String
    var headerTitle : String
    var headerSubtitle : String
}

enum ResultType : Int {
    case Sucess
    case failure
}



